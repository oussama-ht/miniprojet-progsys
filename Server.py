# coding: utf-8
import socket
import pickle
import os
import signal
import traceback
from threading import Thread
import sys

def main():
	start_server()

def start_server():
	print('My PID is:', os.getpid())
	#catchable_sigs = set(signal.signals) - {signal.SIGKILL}
	#for sig in catchable_sigs:
	#	signal.signal(sig, signal.SIG_IGN)
	#signal.signal(SIGKILL, signal.SIG_IGN)


	TCP_IP = '127.0.0.1'
	TCP_PORT = 5005
	BUFFER_SIZE = 1024  # Normally 1024, but we want fast response # taille de message par paquet

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)	# SO_REUSEADDR flag tells the kernel to reuse a local socket in TIME_WAIT state, without waiting for its natural timeout to expire
	print("Socket created")

	try:
		s.bind((TCP_IP, TCP_PORT))
	except:
		print("Bind failed. Error : " + str(sys.exc_info()))
		sys.exit()

	s.listen(5)
	print("Socket now listening")


	#conn, addr = s.accept()
	#print 'Connection address:', addr
	while 1:
		connection, address = s.accept()
		ip, port = str(address[0]), str(address[1])
		print("Connected with " + ip + ":" + port)
		try:
			Thread(target=client_thread, args=(connection, ip, port)).start()
		except:
			print("Thread did not start.")
			traceback.print_exc()

		s.close()

def client_thread(connection, ip, port, BUFFER_SIZE = 5120):
	is_active = True

	while is_active:
		client_input = receive_input(connection, BUFFER_SIZE)

		if "--QUIT--" in client_input:
			print("Client is requesting to quit")
			connection.close()
			print("Connection " + ip + ":" + port + " closed")
			is_active = False
		else:
			print("Processed result: {}".format(client_input))
			connection.sendall("-".encode("utf8"))


def receive_input(connection, BUFFER_SIZE):
	client_input = connection.recv(BUFFER_SIZE)
	client_input_size = sys.getsizeof(client_input)

	if client_input_size > BUFFER_SIZE:
		print("The input size is greater than expected {}".format(client_input_size))

	decoded_input = client_input.decode("utf8").rstrip()  # decode and strip end of line
	result = process_input(decoded_input)

	return result


def process_input(input_str):
	print("Processing the input received from client")

	return "Hello " + str(input_str).upper()


if __name__ == "__main__":
	main()














	'''
	data = conn.recv(BUFFER_SIZE)
	if not data: break
	print "received data:", data
	L= data.count('\n')
	C= len(data)
	M = len(data.split(' '))
	print "le serveur va envoyer le nombre de ligne de fichier :", L
	print "le serveur va envoyer le nombre de charchtère de fichier :", C
	print "le serveur va envoyer le nombre de mot de fichier :", M	
	D = [L,C,M]
	D = pickle.dumps(D)
	conn.send(D)

conn.close()
'''
